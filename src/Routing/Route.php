<?php
namespace Rithmety\Routing;

use Attribute;

#[Attribute(Attribute::TARGET_METHOD | Attribute::TARGET_FUNCTION)]
class Route {
	/**
	 * @psalm-var list<'GET'|'POST'|'DELETE'|'PUT'|'PATCH'> $method
	 */
	protected array $method;
	protected string $path;
	/**
	 * @psalm-param 'ANY'|'GET'|'POST'|'DELETE'|'PUT'|'PATCH'|list<'ANY'|'GET'|'POST'|'DELETE'|'PUT'|'PATCH'> $method
	 */
	function __construct($method, string $path) {
		$this->path = $path;
		$this->method = [];
		foreach (\is_array($method) ? $method : [$method] as $m) {
			$r = match ($m) {
				'ANY' => $this->method = ['GET', 'POST', 'DELETE', 'PUT', 'PATCH'],
				'GET' => $this->method[] = 'GET',
				'POST' => $this->method[] = 'POST',
				'DELETE' => $this->method[] = 'DELETE',
				'PUT' => $this->method[] = 'PUT',
				'PATCH' => $this->method[] = 'PATCH',
			};
			if (\is_array($r)) break;
		}
	}

	/** @return string */
	function getPath() { return $this->path; }

	/** @psalm-return 'GET'|'POST'|'DELETE'|'PUT'|'PATCH'|false */
	function matchMethod(string $method) {
		foreach ($this->method as $m) {
			if ($method === $m) return $m;
		}
		return false;
	}

	/**
	 * @psalm-return array<string,string>|false
	 */
	function matchPath(string $path) {
		$tar = \explode('/', \trim($this->path, '/'));
		$req = \explode('/', \trim($path, '/'));
		if (\count($tar) !== \count($req)) return false;
		$params = [];
		foreach ($tar as $idx => $part) {
			if (\str_starts_with($part, ':')) {
				$name = \substr($part, 1);
				$params[$name] = $req[$idx];
			} elseif ($part !== $req[$idx]) {
				return false;
			}
		}
		return $params;
	}

	/**
	 * @psalm-param array<string,string> $params
	 */
	function createUrl(array $params): string {
		$tar = \explode('/', \trim($this->path, '/'));
		$parts = [''];
		foreach ($tar as $part) {
			if (\str_starts_with($part, ':')) {
				$name = \substr($part, 1);
				$parts[] = $params[$name];
			} else {
				$parts[] = $part;
			}
		}
		return \join('/', $parts);
	}
}
