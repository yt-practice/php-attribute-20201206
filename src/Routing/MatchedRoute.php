<?php
namespace Rithmety\Routing;

use Attribute;

final class MatchedRoute {
	/** @psalm-var 'GET'|'POST'|'DELETE'|'PUT'|'PATCH' $method */
	protected string $method;
	/** @psalm-var string $path */
	protected string $path;
	/** @psalm-var string $url */
	protected string $url;
	/** @psalm-var callable $action */
	protected $action;
	/** @psalm-var array<string,string> $params */
	protected $params;
	/** @psalm-var \ReflectionMethod|\ReflectionFunction $ref */
	protected $ref;
	/** @psalm-var \ReflectionClass|null $cref */
	protected $cref;
	/**
	 * @psalm-param 'GET'|'POST'|'DELETE'|'PUT'|'PATCH' $method
	 * @psalm-param string $path
	 * @psalm-param string $url
	 * @psalm-param callable $action
	 * @psalm-param array<string,string> $params
	 * @psalm-param \ReflectionMethod|\ReflectionFunction $ref
	 * @psalm-param \ReflectionClass|null $cref
	 */
	function __construct($method, $path, $url, $action, $params, $ref, $cref) {
		$this->method = $method;
		$this->path = $path;
		$this->url = $url;
		$this->action = $action;
		$this->params = $params;
		$this->ref = $ref;
		$this->cref = $cref;
	}

	/** @psalm-return 'GET'|'POST'|'DELETE'|'PUT'|'PATCH' */
	function getMethod() { return $this->method; }
	/** @return string */
	function getPath() { return $this->path; }
	/** @return string */
	function getUrl() { return $this->url; }
	/** @psalm-return callable */
	function getAction() { return $this->action; }
	/** @psalm-return array<string,string> */
	function getParams() { return $this->params; }

	function __invoke() {
		return \call_user_func($this->action);
	}
}
