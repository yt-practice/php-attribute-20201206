<?php
namespace Rithmety\Routing;

class HttpException extends \Exception {
	private int $status;
	/**
	 * @param string $message
	 * @param int $code
	 * @param \Throwable|null $previous
	 */
	function __construct(int $status, string $message, $code = 1, ?\Throwable $previous = null) {
		parent::__construct($message, $code, $previous);
		$this->status = $status;
	}

	function getStatus(): int {
		return $this->status;
	}
}
