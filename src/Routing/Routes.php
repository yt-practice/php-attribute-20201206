<?php
namespace Rithmety\Routing;

class Routes {
	private string $dirpath;
	/** @var list<array{name:callable,attr:Route,ref:\ReflectionMethod|\ReflectionFunction,cref:\ReflectionClass|null}>|null */
	private $routes;
	function __construct(string $dirpath) {
		$this->dirpath = $dirpath;
		$this->routes = null;
	}

	function routes(): array {
		return $this->_routes();
	}

	/**
	 * @psalm-return MatchedRoute
	 */
	function matched(string $method, string $path) {
		foreach ($this->_routes() as $route) {
			if ($m = $route['attr']->matchMethod($method) and false !== ($p = $route['attr']->matchPath($path))) {
				return new MatchedRoute($m, $route['attr']->getPath(), $path, $route['name'], $p, $route['ref'], $route['cref']);
			}
		}
		throw new NotFound;
	}

	/**
	 * @psalm-param string|array<string> $controller
	 * @psalm-param array<string,string>|null $params
	 */
	function createUrl($controller, ?array $params = null): string {
		if (\is_string($controller) and \str_contains($controller, '#')) {
			$controller = \explode('#', $controller);
		}
		if (\is_string($controller)) {
			$controller = \strtolower($controller);
		} else {
			$controller = \array_map('strtolower', $controller);
		}
		foreach ($this->_routes() as $route) {
			if (\is_string($route['name'])) {
				$routename = \strtolower($route['name']);
			} elseif (\is_array($route['name'])) {
				/** @psalm-suppress MixedArgument */
				$routename = \array_map(fn($a) => \is_string($a) ? \strtolower($a) : $a, $route['name']);
			} else {
				$routename = $route['name'];
			}
			if ($routename === $controller) return $route['attr']->createUrl($params ?? []);
		}
		throw new \Error('no route was found.');

	}

	private static function _include_dir(string $dir): void {
		foreach (\scandir($dir) as $f) {
			$p = \rtrim($dir, '\\/') . \DIRECTORY_SEPARATOR . $f;
			if (\is_file($p) and \preg_match('/\\.php$/i', $p)) {
				/** @psalm-suppress UnresolvableInclude */
				include_once $p;
			} elseif (\is_dir($p) and !\in_array($f, ['.', '..'], true)) {
				self::_include_dir($p);
			}
		}
	}

	/**
	 * @param callable $name
	 * @param \ReflectionMethod|\ReflectionFunction $ref
	 * @param \ReflectionClass|null $cref
	 */
	private function _push($name, $ref, $cref): void {
		foreach ($ref->getAttributes(Route::class, \ReflectionAttribute::IS_INSTANCEOF) as $at) {
			$attr = $at->newInstance();
			$this->routes[] = \compact('name', 'attr', 'ref', 'cref');
		}
	}

	/**
	 * @psalm-return list<array{name:callable,attr:Route,ref:\ReflectionMethod|\ReflectionFunction,cref:\ReflectionClass|null}>
	 */
	protected function _routes(): array {
		if (null === $this->routes) {
			$this->routes = [];
			$dir = \realpath($this->dirpath);
			self::_include_dir($dir);
			foreach (\get_defined_functions()['user'] as $name) {
				$ref = new \ReflectionFunction($name);
				if (\str_starts_with(\realpath($ref->getFileName()), $dir . \DIRECTORY_SEPARATOR)) {
					$this->_push($name, $ref, null);
				}
			}
			foreach (\get_declared_classes() as $classname) {
				$cref = new \ReflectionClass($classname);
				if (\str_starts_with(\realpath($cref->getFileName()), $dir . \DIRECTORY_SEPARATOR)) {
					foreach ($cref->getMethods() as $ref) {
						$this->_push([$classname, $ref->name], $ref, $cref);
					}
				}
			}
		}
		return $this->routes;
	}

}
