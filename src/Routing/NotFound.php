<?php
namespace Rithmety\Routing;

class NotFound extends HttpException {
	function __construct(string $message = 'not found.') {
		parent::__construct(404, $message);
	}
}
