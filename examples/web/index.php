<?php

require_once __DIR__ . '/../../vendor/autoload.php';

use Rithmety\Routing\Routes;
use Rithmety\Routing\HttpException;

$routes = new Routes(__DIR__ . '/api');

var_export($routes->routes()); echo \PHP_EOL;

var_export($routes->createUrl('App\\hello3', ['name' => '333'])); echo \PHP_EOL;
var_export($routes->createUrl('App\\Hoge#hello2')); echo \PHP_EOL;

/**
 * ```
 * $routes->match($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI'])
 * ```
 */
$matchedRoute = $routes->matched('GET', '/hello3/hoge');
// var_export($matchedRoute); echo \PHP_EOL;

try {
	\var_export($matchedRoute()); echo \PHP_EOL;
} catch (HttpException $th) {
	/**
	 * ```
	 * \http_response_code($th->getStatus());
	 * exit($th->getMessage());
	 * ```
	 */
	\var_export($th); echo \PHP_EOL;
}

