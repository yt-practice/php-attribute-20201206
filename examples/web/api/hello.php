<?php
namespace App;

use Rithmety\Routing\Route;
use Attribute;

#[Attribute]
class Get extends Route {
	function __construct(string $path) {
		parent::__construct(['GET'], $path);
	}
}

#[Route('GET', '/hello')]
function hello() {
	return 'world';
}

#[Get('/hello3/:name')]
function hello_3() {
	return 'world_3';
}

class Hoge {
	#[Route('GET', '/hello2')]
	function hello2() {
		return 'world2';
	}
}
