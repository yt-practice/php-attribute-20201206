# シンプルなルーター

```php
$routes = new Routes(__DIR__ . '/api');

try {
  $matchedRoute = $routes->matched($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);
	exit($matchedRoute());
} catch (HttpException $th) {
	\http_response_code($th->getStatus());
	exit($th->getMessage());
}
```

```php
var_export($routes->createUrl('App\\hello3', ['name' => '333'])); echo \PHP_EOL;
var_export($routes->createUrl('App\\Hoge#hello2')); echo \PHP_EOL;
```
